from fastapi import FastAPI
import datetime
import pickle

app = FastAPI()


@app.get("/")
async def root(name: str = "Stranger"):
    return {
        "message": f"Hello {name}!",
        "timestamp": datetime.datetime.now(),
    }


@app.get("/predict")
async def predict(text: str):
    with open("model.p", "rb") as f:
        pipe = pickle.load(f)
    result = pipe.predict_proba([text])
    return {"text": text, "result": result[0, 1]}


with open("model.p", "rb") as f:
    model = pickle.load(f)


@app.get("/predict_fast")
async def predict_fast(text: str):
    result = model.predict_proba([text])
    return {"text": text, "result": result[0, 1]}